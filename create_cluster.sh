#!/bin/bash
## Variables
PROJECT_ID=k8stest-326518
PROJECT_NAME=k8stest
REPO_NAME=$PROJECT_NAME
CLUSTER_NAME=$PROJECT_NAME
NUM_NODES=3
REGION=europe-west3
ZONE=europe-west3-a
KEY_FILE=key.json
WEB_APP_NAME=webserver


## Install tools
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
sudo apt-get update && sudo apt-get install google-cloud-sdk kubectl

## Setup GCP
gcloud services enable container.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable artifactregistry.googleapis.com

gcloud auth activate-service-account --key-file=$KEY_FILE
gcloud config set project $PROJECT_ID
gcloud config set compute/zone $ZONE
gcloud config set compute/region $REGION

## Create GKE cluster
gcloud container clusters create $PROJECT_NAME \
--tags=k8s-test-cluster \
--num-nodes $NUM_NODES

## Create docker repository
gcloud artifacts repositories create $CLUSTER_NAME \
   --repository-format=docker \
   --location=$REGION \
   --description="$PROJECT_NAME docker repository"

## Prepare web app
cd webserver
docker build -t $REGION-docker.pkg.dev/${PROJECT_ID}/${REPO_NAME}/${WEB_APP_NAME}:v1 .
#docker run --rm -p 8080:80 $REGION-docker.pkg.dev/${PROJECT_ID}/${REPO_NAME}/webserver:v1
gcloud auth configure-docker $REGION-docker.pkg.dev
docker push $REGION-docker.pkg.dev/${PROJECT_ID}/${REPO_NAME}/${WEB_APP_NAME}:v1

gcloud container clusters get-credentials $CLUSTER_NAME --zone $ZONE
kubectl apply -f deployment.yaml

## Deploy ELK
kubectl create clusterrolebinding cluster-admin-binding --clusterrole=cluster-admin --user=dan
kubectl create clusterrolebinding dan-cluster-admin-binding --clusterrole=cluster-admin --user=dan

kubectl apply -f https://download.elastic.co/downloads/eck/1.8.0/all-in-one.yaml


echo `kubectl get secret elasticsearch-sample-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode`
kubectl expose deployment svc/elastic-gke-logging-1-kibana-svc --type=LoadBalancer --port 5601
kubectl expose deployment elastic-gke-logging-1-kibana --type=LoadBalancer --port 5601

https://medium.com/@sece.cosmin/docker-logs-with-elastic-stack-elk-filebeat-50e2b20a27c6
